#!/bin/bash
####################################################################
# Copyright (c) 2001-2017 by SAP AG, Walldorf, Germany.
#
# Native application startup script:
# - Creates a SAP WebDispatcher profile out of a template file
# - Starts SAP WebDispatcher as webserver for the native application
# - Prepares a Unix Domain socket 
# - Starts the native application
####################################################################

# ----------------------------------------------------
# Init 
# ----------------------------------------------------

echo Initializing native application startup...

CURRENT_WORKING_DIR=`pwd`
TAG_FILE_NAME="./nativeContainer"
WEBSERVER_DIR=$CURRENT_WORKING_DIR/webserver
START_COMMAND=`cat "$CURRENT_WORKING_DIR/nativeContainer"`
CURRENT_USER=`whoami`
UDS_PATH=/tmp/.sapicm.$CURRENT_USER.
UNIX_DOMAIN_SOCKET=$UDS_PATH$VCAP_APP_PORT

echo
echo Configuration:
echo "  Internal port      :" $VCAP_APP_PORT
echo "  CWD                :" $CURRENT_WORKING_DIR
echo "  Home directory     :" $HOME
echo "  User name          :" $CURRENT_USER
echo "  UDS path           :" $UDS_PATH
echo "  Unix domain socket :" $UNIX_DOMAIN_SOCKET
echo "  Start command      :" $START_COMMAND
echo

# Check that a port is given
if [ -z "${VCAP_APP_PORT}" ]; then
	   echo "##################################################################"
	      echo "ERROR: Missing application port in environment (set VCAP_APP_PORT)"
	         echo "##################################################################" 
		    exit 1
	    fi

	    # ----------------------------------------------------
	    # Start the web server 
	    # ----------------------------------------------------

	    # Profile locations
	    WEBSERVER_CONF_TEMPLATE=$WEBSERVER_DIR/conf/sapwebdisp.pfl.template
	    WEBSERVER_CONF_FILE=$WEBSERVER_DIR/conf/sapwebdisp.pfl

	    # Generate actual profile out of template
	    sed -e "s|\${VCAP_APP_PORT}|$VCAP_APP_PORT|g" -e "s|\${BASE_DIR}|$WEBSERVER_DIR|g" -e "s|\${UDS_PATH}|${UDS_PATH}|g" "$WEBSERVER_CONF_TEMPLATE" > "$WEBSERVER_CONF_FILE"

	    # Start SAP WebDispatcher
	    cd "$WEBSERVER_DIR"
	    exec ./sapwebdisp "pf=$WEBSERVER_CONF_FILE" -f "$WEBSERVER_DIR/logs/dev_trace" 1> $WEBSERVER_DIR/logs/stdout.log 2> $WEBSERVER_DIR/logs/stderr.log &

	    echo Started SAP WebDispatcher with log file location \'$WEBSERVER_DIR/logs\'

	    # ----------------------------------------------------
	    # Start the fcgi server (e.g. the native application)
	    # ----------------------------------------------------

	    echo Starting native application...
	    cd "$CURRENT_WORKING_DIR"
	    LD_LIBRARY_PATH=. exec ./spawn-fcgi -n -s $UNIX_DOMAIN_SOCKET $START_COMMAND
	    exit $?
