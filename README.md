# Manually Create a Base Buildpack #

Get the simple web server found [here](https://www.ibm.com/developerworks/systems/library/es-nweb/).

SCP it to your Linux server running XS-A
```
#!bash
scp nweb.zip ragi:/home/i830671
```

Unzip and compile.
```
#!bash
unzip nweb.zip

cc -O2 nweb23.c -o nweb
```

Perform this from the parent directory of this repository.
```
#!bash
xs create-buildpack base_buildpack xsa_base_buildpack 9
```

```
#!bash
xs delete-buildpack base_buildpack -f --quiet
```

```
#!bash
```
